#include <array>
#include <utility>
#include <string>
#include <fstream>

struct Color{
  short r;
  short g;
  short b;
};

template <size_t X, size_t Y>
class Image{
  std::array<std::array<Color, X>, Y> data;
  public:

  Image(){
    data = {};
  };

  std::array<Color, X>& operator[](size_t index){
    return data[index];
  }

  std::pair<size_t, size_t> size() const{
    return {X, Y};
  }
  void save(std::string filename){
    std::ofstream file(filename);
    file << "P3\n";
    file << X << " " << Y << "\n255";
    for(auto line : data){
      file << "\n";
      for(auto pixel : line){
        file << pixel.r << " " << pixel.g << " " << pixel.b<<" ";
      }
    }
  }
};
