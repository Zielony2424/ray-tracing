#include <array>
#include <cmath>
#include <iostream>
#include <numeric>
#include <algorithm>
#include <cassert>

template <typename T, size_t size_>
class Vector{
  std::array<T, size_> data;
  public:
  Vector(std::initializer_list<T> initList){
    assert(initList.size() == size_);
    std::copy(initList.begin(), initList.end(), data.begin());
  }

  Vector(std::array<T, size_> initArray){
    std::copy(initArray.begin(), initArray.end(), data.begin());
  }

  Vector() = default;

  Vector<T, size_> operator+(const Vector<T, size_>& v) const{
    Vector result;
    std::transform(data.begin(), data.end(), v.data.begin(), result.data.begin(), std::plus<int>());
    return result;
  }

  Vector<T, size_> operator-(const Vector<T, size_>& v) const{
    Vector result;
    std::transform(data.begin(), data.end(), v.data.begin(), result.data.begin(), std::minus<int>());
    return result;
  }

  T operator[](size_t index) const{
    return data[index];
  }

  T norm() const{
    return std::sqrt(this->dot(*this));
  }

  Vector<T, size_> normalized() const{
    return *this*(1/this->norm());
  }

  T dot(const Vector<T, size_> v) const{
    return std::inner_product(data.begin(), data.end(), v.data.begin(), T());
  }

  template <typename S, size_t size__>
  friend std::ostream& operator<<(std::ostream& stream, const Vector<S, size__>& v);

  template <typename S, size_t size__>
  friend Vector<S, size__> operator*(const Vector<S, size__> v, const S scalar);
};

template <typename T, size_t size_>
Vector<T, size_> operator*(const Vector<T, size_> v, const T scalar){
  Vector<T, size_> result;
  std::transform(v.data.begin(), v.data.end(), result.data.begin(), [scalar](auto& c){return c*scalar;});
  return result;
}

template <typename T, size_t size_>
Vector<T, size_>& operator*(const T scalar, const Vector<T, size_> v){
  return v*scalar;
}

template <typename T, size_t size_>
std::ostream& operator<<(std::ostream& stream, const Vector<T, size_>& v){
  stream << "[ ";
  for(const auto& i : v.data){
    stream << i << ", ";
  }
  stream<<" ]";
  return stream;
}


