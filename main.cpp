#include "Vector.h"
#include "Image.h"
#include <iostream>
#include <string>


using vec3d = Vector<double, 3>;
int main(){
  constexpr size_t X = 1024;
  constexpr size_t Y = 800;
  Image<X, Y> img;
  Color red = {255, 0, 0};
  vec3d S = {0, 0, 20};
  double r = 19.9;
  for(int y = 0; y < Y; ++y){
    for(int x = 0; x < X; ++x){
      vec3d plane = {static_cast<double>(x)-X/2, static_cast<double>(y)-Y/2, 10};
      plane = plane.normalized();
      bool breakFlag = false;
      for(double t = 0.1; t<70 && !breakFlag; t+=0.1){
        if(auto v = plane*t - S; v.norm()<r){
          img[x][y] = {100-t, 255-t*25, 0};
          breakFlag = true;
        }
        if(auto v = (plane*t); v[0]<0){
          img[x][y] = {255-t*50, 255-t*50, 30};
          breakFlag = true;
        }
      }
    }
  }
  img.save("test.ppm");

}
